import static io.appium.java_client.touch.LongPressOptions.longPressOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static java.time.Duration.ofSeconds;

import java.net.MalformedURLException;
import java.util.HashMap;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.touch.offset.PointOption;

public class IOTest extends base{

	@SuppressWarnings("rawtypes")
	public static void main(String[] args) throws MalformedURLException, InterruptedException {
		// TODO Auto-generated method stub

		IOSDriver<IOSElement> driver = Capabilities();
		
		IOSElement alert = driver.findElementByAccessibilityId("Alert Views");
		alert.click();
		driver.findElementByName("Text Entry").click();
		driver.findElementByClassName("XCUIElementTypeTextField").sendKeys("mierdaaaaaaaaa");
				
		driver.findElementByName("OK").click();
		driver.navigate().back();
		
		Thread.sleep(2000);
		
		Dimension size = driver.manage().window().getSize(); 
		
		TouchAction t = new TouchAction(driver);
		
		int x = size.getWidth()/2;
		int starty = (int)(size.getHeight()*0.60);
		int endy = (int)(size.getHeight()*0.10);
		
		t.longPress(longPressOptions().withPosition(PointOption.point(x,starty)).withDuration(ofSeconds(2))).moveTo(PointOption.point(x,endy)).release().perform();
		
		driver.findElementByAccessibilityId("Steppers").click();
		
		driver.findElementsByName("Increment").get(0).click();
		driver.findElementsByName("Increment").get(0).click();
		driver.findElementsByName("Increment").get(1).click();
		driver.findElementsByName("Increment").get(2).click();
		
		driver.findElementsByName("Decrement").get(0).click();
		driver.findElementsByName("Decrement").get(2).click();
		
		driver.navigate().back();
		Thread.sleep(2000);
	
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap<String, String> scrollObject = new HashMap<>();
		scrollObject.put("direction", "up");
		scrollObject.put("name", "Activity Indicators");
		js.executeScript("mobile: scroll", scrollObject);
		
		driver.findElementByName("Picker View").click();
		WebElement el = driver.findElementsByClassName("XCUIElementTypePickerWheel").get(2);
		
		t.longPress(longPressOptions().withElement(element(el))).moveTo(PointOption.point(200, 150)).release().perform();		
	}
}
