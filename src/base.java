import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;


public class base {
	
	static IOSDriver<IOSElement> driver;

	@SuppressWarnings("unchecked")
	public static IOSDriver<IOSElement> Capabilities() throws MalformedURLException {
		// TODO Auto-generated method stub

		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 6");
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "IOS");
		cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, "12.0");
		
		cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
		
		File f = new File("src");
		File fs = new File(f,"UICatalog.app");
		cap.setCapability(MobileCapabilityType.APP, fs.getAbsolutePath());
		
		//cap.setCapability(MobileCapabilityType.APP, "/Users/jesusurbaez/Desktop/UICatalog.app");
		
		@SuppressWarnings("rawtypes")
		IOSDriver driver = new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		
		return driver;
	}
}
